//
//  Source/Headers/SpringBoard/SBDeleteIconAlertItem.h
//
//  Created by inoahdev on 12/26/16
//  Copyright © 2016 inoahdev. All rights reserved.
//

#ifndef SPRINGBOARD_SBDELETEICONALERTITEM_H
#define SPRINGBOARD_SBDELETEICONALERTITEM_H

#import "../SpringBoardUI/SBAlertItem.h"

@interface SBDeleteIconAlertItem : SBAlertItem
@end

#endif
