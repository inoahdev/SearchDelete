//
//  Source/Headers/UIKit/UIAlertController.h
//
//  Created by inoahdev on 12/25/16
//  Copyright © 2016 inoahdev. All rights reserved.
//

#ifndef UIKIT_UIALERTCONTROLLER_H
#define UIKIT_UIALERTCONTROLLER_H

#import <UIKit/UIKit.h>

@interface UIAlertController ()
@property (nonatomic, strong) UIAlertAction *_cancelAction;
@end

#endif
