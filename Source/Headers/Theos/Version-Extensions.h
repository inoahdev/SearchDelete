//
//  Source/Headers/Theos/Version-Extensions.h
//
//  Created by inoahdev on 12/25/16
//  Copyright © 2016 inoahdev. All rights reserved.
//

#ifndef THEOS_VERSION_EXTENSIONS_H
#define THEOS_VERSION_EXTENSIONS_H

#import <version.h>

#ifndef kCFCoreFoundationVersionNumber_iOS_10
#define kCFCoreFoundationVersionNumber_iOS_10 1151.16
#endif

#ifndef kCFCoreFoundationVersionNumber_iOS_10_1_1
#define kCFCoreFoundationVersionNumber_iOS_10_1_1 1348
#endif

#endif
