//
//  Source/Headers/SearchUI/SearchUIThumbnailView.h
//
//  Created by inoahdev on 12/25/16
//  Copyright © 2016 inoahdev. All rights reserved.
//

#ifndef SEARCHUI_SEARCH_UI_THUMBNAIL_VIEW_H
#define SEARCHUI_SEARCH_UI_THUMBNAIL_VIEW_H

#import <UIKit/UIKit.h>

@interface SearchUIThumbnailView : UIView
@property (nonatomic, strong) UIImageView *imageView;
@end

#endif
